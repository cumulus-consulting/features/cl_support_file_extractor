#!/usr/bin/env python

import os
import sys
import glob
import shutil
import tarfile
import subprocess

# Constants
LIST_OF_FILES = {'/etc/network/interfaces',
                 '/etc/frr/daemons',
                 '/etc/frr/frr.conf',
                 '/etc/hostname'}
FILE_DIR = 'backup_files'

# Get current dir
cwd = os.getcwd()

list_of_tarchives = glob.glob(cwd+'/*.txz')

# Unzip all the cl_supports
for tarball in list_of_tarchives:
    print("Extracting: {}".format(tarball))
    try:
        subprocess.check_call(['tar','xf',tarball],)
    except:
        print("Could not extract: {}".format(tarball))
        sys.exit(1)

# Find the hostname of each cl-support
# Create a directory with the hostname
# Copy over relevant files based on constant
for tarball in list_of_tarchives:
    folder = tarball[:-4]
    print("folder is: {}".format(folder))
    f = open(folder + '/etc/hostname', 'r')
    hostname = f.readlines()[0][:-1]
    print(hostname)

    # only create directory if it doesn't exist
    if os.path.exists(FILE_DIR + '/' + hostname):
        print("Directory already exists: " + FILE_DIR + '/' + hostname)
    else:
        os.makedirs(FILE_DIR + '/' + hostname)

    # copy over files, retain only their file name
    for file in LIST_OF_FILES:
        shutil.copy(folder+'/'+file,
                    FILE_DIR+'/'+hostname+'/'+file.split('/')[-1])

# Remove Untarred Files
for tarball in list_of_tarchives:
    folder = tarball[:-4]
    shutil.rmtree(folder)