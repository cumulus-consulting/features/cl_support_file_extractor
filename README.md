# CL_Support_File_Extractor

Change to the directory where all cl-supports are held. Ensure cl-supports are still in their original .txz format.

Run the script. The script itself can be in any location, but the current working directory where the script is run must be where teh cl_supports are stored:

```
python3 cl_support_file_extractor.py
```

Once this is done, a directory named `backup_files` will be created.